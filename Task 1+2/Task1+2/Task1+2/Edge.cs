﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1_2
{
    public class Edge
    {
        public Dictionary<char, Dictionary<char, int>> edges = new Dictionary<char, Dictionary<char, int>>();

        //public void addEdge(char vertex1, Dictionary<char, int> edgesWithCost)
        //{
        //    edges[vertex1] = edgesWithCost;
        //}

        public void addEdge(char name, Dictionary<char, int> edgesWithCost)
        {
            edges[name] = edgesWithCost;
        }

        public List<char> DjikstrasAlgorithm(char start, char finish)
        {
            // List of vertices in graph
            var vertices = new List<char>();
            // List to store shortest path in
            List<char> path = new List<char>();
            // List of previously visited vertex
            var prev = new Dictionary<char, char>();
            // List of vertex and cost
            var dist = new Dictionary<char, int>();
            
            // Looping through all the edges
            foreach (var e in edges)
            {
                // If the edge is the same as the source
                if (e.Key == start)
                {
                    // Distance is given as 0 as there is no cost and stored in the distance dictionary alongside the vertex
                    dist[e.Key] = 0;
                }
                else
                {
                    // Distance is given a max value and stored in the distance dictionary alongside the vertex
                    dist[e.Key] = int.MaxValue;
                }

                // Adding the vertex visited to a list of vertices
                vertices.Add(e.Key);
            }

            // While there are vertices
            while (vertices.Count != 0)
            {
                // Sorts the vertices list with the smallest cost first
                vertices.Sort((x, y) => dist[x] - dist[y]);

                // Assigning the smallest variable to the first element in the vertices list as after sorting it should be the smallest
                var smallest = vertices[0];
                // Removing the smallest vertex from the vertices list
                vertices.Remove(smallest);

                // If the smallest vertex is the same as the end vertex passed as a parameter to the method
                if (smallest == finish)
                {
                    // as long as the smallest vertex is found in the previous list
                    while (prev.ContainsKey(smallest))
                    {
                        // Adding the smallest vertex to the path
                        path.Add(smallest);
                        // Assigning the smallest variable to the smallest variable found in the previous list
                        smallest = prev[smallest];
                    }

                    // Exit
                    break;
                }

                // If the cost of the smallest value in the distance list is equal to the maxValue of int
                if (dist[smallest] == int.MaxValue)
                {
                    // Exit 
                    break;
                }

                // For all the vertex connected with edges to the smallest vertex
                foreach (var neighbor in edges[smallest])
                {
                    // Assigning the alternate variable to be equal to the cost of the smallest value added onto the cost of the neighbouring edge
                    var alt = dist[smallest] + neighbor.Value;

                    // If the alternate variable is smaller than the cost of the neighbouring edge
                    if (alt < dist[neighbor.Key])
                    {
                        // Assigning the new cost of the neighbor to the alternate variable
                        dist[neighbor.Key] = alt;
                        // Assigning the previous vertex to the smallest vertex
                        prev[neighbor.Key] = smallest;
                    }
                }
            }

            // Returning the list containing the paths
            return path;
        }
    }
}
