﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1_2
{
    class Program
    {
        public static void Main(string[] args)
        {
            // Creating the graph with costs and edges
            #region Creating the graph
            Edge e = new Edge();
            e.addEdge('A', new Dictionary<char, int>() { { 'B', 5 }, { 'C', 3 }, { 'E', 2 }, { 'F', 1 } });
            e.addEdge('B', new Dictionary<char, int>() { { 'A', 5 }, { 'D', 4 }, { 'G', 3 }, { 'H', 1 } });
            e.addEdge('C', new Dictionary<char, int>() { { 'A', 3 }, { 'D', 2 } });
            e.addEdge('D', new Dictionary<char, int>() { { 'B', 4 }, { 'C', 2 }, { 'I', 1 }, { 'J', 2 } });
            e.addEdge('E', new Dictionary<char, int>() { { 'A', 2 }, { 'I', 3 } });
            e.addEdge('F', new Dictionary<char, int>() { { 'A', 1 }, { 'I', 2 }, { 'J', 4 } });
            e.addEdge('G', new Dictionary<char, int>() { { 'B', 3 } });
            e.addEdge('H', new Dictionary<char, int>() { { 'B', 1 } });
            e.addEdge('I', new Dictionary<char, int>() { { 'D', 1 }, { 'E', 3 }, { 'F', 2 } });
            e.addEdge('J', new Dictionary<char, int>() { { 'D', 2 }, { 'F', 4 } });
            #endregion

            // Displaying created graph with all corresponding edges
            Console.WriteLine("Graph with all edges and cost (Contains all edges for each node, even if already shown \n ");
            foreach(var x in e.edges)
            {
                foreach(var y in x.Value)
                {
                    Console.WriteLine("{0} ----> {1} -- Cost: {2}", x.Key, y.Key, y.Value);
                }
            }

            Console.ReadKey();
            Console.Clear();

            // Calling the Algorithm to find shortest paths
            List<char> AToB = e.DjikstrasAlgorithm('A', 'B');
            List<char> AToC = e.DjikstrasAlgorithm('A', 'C');
            List<char> AToD = e.DjikstrasAlgorithm('A', 'D');
            List<char> AToE = e.DjikstrasAlgorithm('A', 'E');
            List<char> AToF = e.DjikstrasAlgorithm('A', 'F');
            List<char> AToG = e.DjikstrasAlgorithm('A', 'G');
            List<char> AToH = e.DjikstrasAlgorithm('A', 'H');
            List<char> AToI = e.DjikstrasAlgorithm('A', 'I');
            List<char> AToJ = e.DjikstrasAlgorithm('A', 'J');

            // Displaying shortest paths for all vertices from source (A)
            displayAllShortestPaths(AToB, 'A', 'B');
            displayAllShortestPaths(AToC, 'A', 'C');
            displayAllShortestPaths(AToD, 'A', 'D');
            displayAllShortestPaths(AToE, 'A', 'E');
            displayAllShortestPaths(AToF, 'A', 'F');
            displayAllShortestPaths(AToG, 'A', 'G');
            displayAllShortestPaths(AToH, 'A', 'H');
            displayAllShortestPaths(AToI, 'A', 'I');
            displayAllShortestPaths(AToJ, 'A', 'J');

            Console.ReadKey();
        }

        // Method to loop through list returned and display shortest paths
        public static void displayAllShortestPaths(List<char> l, char start, char end)
        {
            Console.Write("Path from {0} to {1}: ", start, end);
            Console.Write("A ");
            foreach (var x in l.AsEnumerable().Reverse())
            {
                Console.Write(x + " ");
            }

            Console.WriteLine();
        }
    }
}
